import React, { Fragment } from "react";

const Index = () => {
  return (
    <Fragment>
      <h1>Sup bruh? Click one of the links above.</h1>
      <img
        src="https://media.giphy.com/media/3ohhwhbQ1MpyZrWges/giphy-downsized.gif"
        alt="sup-bruh"
        className="sup"
      />
    </Fragment>
  );
};

export default Index;
