import React, { Fragment } from "react";

import Table from "../layouts/Table/Table";

const FCasino = () => {
  return (
    <Fragment>
      <h1>Foxy Casino</h1>
      <Table url="https://casino.foxycasino.com/en/games/api/content/GetGameMetaDataFromLMT" />
    </Fragment>
  );
};

export default FCasino;
