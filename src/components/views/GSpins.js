import React, { Fragment } from "react";

import Table from "../layouts/Table/Table";

const GSpins = () => {
  return (
    <Fragment>
      <h1>Gala Spins</h1>
      <Table url="https://www.galaspins.com/en/games/api/content/GetGameMetaDataFromLMT" />
    </Fragment>
  );
};

export default GSpins;
