import React, { Fragment } from "react";

import Table from "../layouts/Table/Table";

const FBingo = () => {
  return (
    <Fragment>
      <h1>Foxy Bingo</h1>
      <Table url="https://casino.foxybingo.com/en/games/api/content/GetGameMetaDataFromLMT" />
    </Fragment>
  );
};

export default FBingo;
