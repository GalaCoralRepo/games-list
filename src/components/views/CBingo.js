import React, { Fragment } from "react";

import Table from "../layouts/Table/Table";

const CBingo = () => {
  return (
    <Fragment>
      <h1>Cheeky Bingo</h1>
      <Table url="https://casino.cheekybingo.com/en/games/api/content/GetGameMetaDataFromLMT" />
    </Fragment>
  );
};

export default CBingo;
