import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

// Import views
import Index from "./views/Index";
import CBingo from "./views/CBingo";
import FBingo from "./views/FBingo";
import FCasino from "./views/FCasino";
import GSpins from "./views/GSpins";

// Layouts
import Navbar from "./layouts/Navbar/Navbar";

function App() {
  return (
    <Router>
      <Navbar />
      <div className="container">
        <Switch>
          <Route exact path="/" component={Index} />
          <Route path="/gspins" component={GSpins} />
          <Route path="/fbingo" component={FBingo} />
          <Route path="/fcasino" component={FCasino} />
          <Route path="/cbingo" component={CBingo} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
