import React, { Component } from "react";

import "./Table.css";

class Table extends Component {
  state = {
    data: [],
    filteredData: []
  };

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    fetch(`https://cors-anywhere.herokuapp.com/${this.props.url}`)
      .then(res => res.json())
      .then(data => {
        this.setState({ data, filteredData: data });
      })
      .catch(err => console.error(err));
  };

  filter = e => {
    let data = this.state.data.filter(item => {
      return (
        item.name.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1
      );
    });
    this.setState({ filteredData: data });
  };

  render() {
    return (
      <div>
        <input
          placeholder="Search for a game name..."
          onChange={e => this.filter(e)}
        />
        <table>
          <thead>
            <tr>
              <th>Game Name</th>
              <th>Game Code</th>
              <th>Game ID</th>
            </tr>
          </thead>
          <tbody>
            <TableBody state={this.state} />
          </tbody>
        </table>
      </div>
    );
  }
}

const TableBody = ({ state }) =>
  state.data.length > 0 ? (
    state.filteredData.length > 0 ? (
      state.filteredData.map((item, index) => {
        return (
          <tr key={index}>
            <td>{item.name}</td>
            <td>{item.game}</td>
            <td>{item.sid.replace("/id/", "")}</td>
          </tr>
        );
      })
    ) : (
      <tr>
        <td colspan="3">
          <strong>Nope... Found nothing, my dude.</strong>
        </td>
      </tr>
    )
  ) : (
    <tr>
      <td colspan="3">
        <div class="lds-hourglass" />
      </td>
    </tr>
  );

export default Table;
