import React from "react";
import { NavLink } from "react-router-dom";

import logo from "../../../assets/logo.png";
import "./Navbar.css";

const Navbar = () => {
  return (
    <nav>
      <img src={logo} alt="gvc-logo" />
      <div>
        <NavLink to="/gspins">Gala Spins</NavLink>
        <NavLink to="/fbingo">Foxy Bingo</NavLink>
        <NavLink to="/fcasino">Foxy Casino</NavLink>
        <NavLink to="/cbingo">Cheeky Bingo</NavLink>
      </div>
    </nav>
  );
};

export default Navbar;
